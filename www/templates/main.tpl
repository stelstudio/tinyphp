<!DOCTYPE html>
<html>
  <head>
    <title><?=$page->title?></title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Template for Bootstrap</title>

      <!-- Bootstrap core CSS -->
      <link href="/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
    <div>User: <?=\Tiny\User\CurrentUser::name()?> <a href="/auth/?logout=1">logout</a> </div>
    <?=$page->content?>
    <hr/>
    Copyright
  </body>
</html>