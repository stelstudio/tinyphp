<?php

session_name('sid');
session_start();

try {
    require_once 'init.php';
}
catch (Exception $e) {
    die("Application not configured properly: ".$e->getMessage());
}

try {
    require_once 'router.php';
}
catch (HttpException $e) {
    echo $e->getMessage();
    exit(1);
}

?>
