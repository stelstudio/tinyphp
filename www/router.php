<?php
Route::startswith('/auth/')->name('auth')->controller('authentication')->dispatch();

Route::startswith('/editor/')->controller('editor')->protect(new MustLogin())->dispatch();

Route::landing('welcome');