<?php
use Tiny\Controller\Base;
use Tiny\Page;
use Tiny\Router\Route;

class WelcomeController extends Base
{
	function __default()
	{
		$page = new Page("main", "Welcome page");
		$page->render(function() {
			$link = Route::find('auth')->url();
			echo "<a href='$link'>Login</a><br/><a href='/editor/'>Editor</a>";
		});
	}
	
}