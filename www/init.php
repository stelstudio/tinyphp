<?
define('BASE_DIR', realpath(__DIR__.'/..'));
define('VENDOR_DIR', BASE_DIR.'/vendor');

use TinyPHP\Settings;
use TinyPHP\CurrentUser;

/**
 * Load settings
 */
Settings::init(BASE_DIR.'/config.php');

if (Settings::production())
{
    ini_set('display_errors', 0);
    error_reporting(E_ERROR);
}
else
{
    ini_set('display_startup_errors',1);
    ini_set('display_errors', 1);
    error_reporting(E_ERROR | E_WARNING);
}

CurrentUser::init('user_id');

