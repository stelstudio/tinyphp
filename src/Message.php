<?php

namespace Tiny;

use Tiny\Response\HTML;

/**
 * Site messages
 */
class Message {
    const SESSION_KEY = 'flash_messages';
    const SUCCESS = 'success';
    const INFO    = 'info';
    const WARNING = 'warning';
    const ERROR   = 'danger';

    private static $initialized = false;

    static function registerToPage()
    {
        // Check if already initialized
        if (self::$initialized)
        {
            return;
        }
        self::$initialized = true;

        $session_key = self::SESSION_KEY;

        HTML::registerBlock('messages', function() use ($session_key) {
            Template::render('messages.tpl', array('messages' => $_SESSION[$session_key]));
            unset($_SESSION[$session_key]);
        });
    }

    static function add($message, $type, $redirect_url = '')
    {
        if (!is_array($_SESSION[self::SESSION_KEY]))
        {
            $_SESSION[self::SESSION_KEY] = array();
        }

//        if (!is_array($_SESSION[self::SESSION_KEY][$type]))
//        {
//            $_SESSION[self::SESSION_KEY][$type] = array();
//        }

        $_SESSION['flash_messages'][] = compact('type', 'message');

        if ($redirect_url)
        {
            header("Location: $redirect_url");
            exit(0);
        }
    }

    static function addSuccess($msg, $redirect_url = '')
    {
        self::add($msg, self::SUCCESS, $redirect_url);
    }

    static function addInfo($msg, $redirect_url = '')
    {
        self::add($msg, self::INFO, $redirect_url);
    }

    static function addWarning($msg, $redirect_url = '')
    {
        self::add($msg, self::WARNING, $redirect_url);
    }

    static function addError($msg, $redirect_url = '')
    {
        self::add($msg, self::ERROR, $redirect_url);
    }
} 