<?php
namespace Tiny;

class Image
{
	var $filename;
	var $destination;
	var $format;
	var $method;
	var $size;
	private $image;
	var $dest_image;
	var $extension;
	var $modified;

	static function getMimeType($filename)
	{
		$finfo = new \finfo(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
		$mime = $finfo->file($finfo, $filename);
	}

	function __construct($filename, $type='', $width=150, $height=150)
	{
		$this->modified  = false;

		$this->filename  = $filename;

		// Autodetect file type if not given
		$this->extension = ($type)  ?  $type  :  strtolower(end(explode('.', $filename)));

		switch($this->extension)
		{
			case 'jpg':
			case 'jpeg':
				$this->image = imagecreatefromjpeg($this->filename);  break;

			case 'png':
				$this->image = imagecreatefrompng($this->filename);   break;

			case 'gif':
				$this->image = imagecreatefromgif($this->filename);   break;

			case 'bmp':
				$this->image = imagecreatefrombmp($this->filename);  break;

			// Create new blank image
			default:
				$this->image = imagecreatetruecolor($width, $height);
				imagefill($this->image, $width, $height, imagecolorallocate($this->image, 255, 255, 255));
				$this->modified = true;
		}

	}

    function __toString()
    {
        if (!$this->filename)
        {
            return;
        }

        $fp = fopen($this->filename, "rb", 0);
        if (!$fp)
        {
            return false;
        }

        $gambar = fread($fp, filesize($this->filename));
        fclose($fp);

        $base64 = chunk_split(base64_encode($gambar));
        return "data:$this->mime;base64,$base64";
    }


	function getWidth()
	{
		return imagesx($this->image);
	}

	function getHeight()
	{
		return imagesy($this->image);
	}


	function resizeImage($maxW = null, $maxH = null, $square = false)
	{
		if ($maxW !== null)
		{
			$this->size['x'] = $maxW;
		}

		if ($maxH !== null)
		{
			$this->size['y'] = $maxH;
		}

		if(imagesx($this->image) > $this->size['x'] || imagesy($this->image) > $this->size['y'])
		{
			$ratioX = imagesx($this->image) / $this->size['x'];
			$ratioY = imagesy($this->image) / $this->size['y'];
			$ratio = max($ratioY, $ratioX);

			$new_w = floor(imagesx($this->image) / $ratio);
			$new_h = floor(imagesy($this->image) / $ratio);

			$this->dest_image = imagecreatetruecolor($new_w, $new_h);

			if ($this->extension == 'gif')
			{
				$transparent_index = imagecolortransparent($this->image);
				if ($transparent_index >= 0)
				{
			        $transparent_color = imagecolorsforindex($this->image, $transparent_index);
			        $transparent_index = imagecolorallocate($this->dest_image, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
			        imagefill($this->dest_image, 0, 0, $transparent_index);

			        imagecolortransparent($this->dest_image, $transparent_index);
				}
			}
			else
			{
				// Transparency
				imagealphablending($this->dest_image, false);
				imagesavealpha($this->dest_image, true);
			}
			$transparent = imagecolorallocatealpha($this->dest_image, 255, 255, 255, 127);
			//imagefilledrectangle($this->dest_image, 0, 0, $new_w, $new_h, $transparent);

			// Resize
			imagecopyresampled($this->dest_image, $this->image, 0, 0, 0, 0, $new_w, $new_h, imagesx($this->image), imagesy($this->image));

			$this->modified = true;
		}
		else
		{
			$this->dest_image = $this->image;
		}
	}

	function cropAndResize($left, $top, $width, $height, $new_width = 0, $new_height = 0)
	{
        if (!$new_width) {
            $new_width = $width;
        }

        if (!$new_height) {
            $new_height = $height;
        }


		// Skip images smaller then destination
		if ($this->getWidth() < $new_width && $this->getHeight() < $new_height)
		{
			$this->dest_image = $this->image;
			return ;
		}

		$this->dest_image = imagecreatetruecolor($new_width, $new_height);
		imagealphablending($this->dest_image, true);
		imagecopyresampled($this->dest_image, $this->image, 0, 0, $left, $top, $new_width, $new_height, $width, $height);

		$this->modified = true;
	}

    /**
     * Add png watermark
     * @param $src
     * @param $top_offset - percents to move down from center
     * @param $transparency
     */
    function watermark($src, $top_offset=1, $transparency=50)
    {
        $wm = imagecreatefrompng($src);
        //imagealphablending($wm, true);
        $black = imagecolorallocate($wm, 0, 0, 0);
        imagecolortransparent($wm, $black);
        $width = imagesx($wm);
        $height = imagesy($wm);

        $dest_x = (imagesx($this->dest_image) - $width) / 2;
        $dest_y = (imagesy($this->dest_image) - $height) / 2 * $top_offset;

        imagecopymerge($this->dest_image, $wm, $dest_x, $dest_y, 0, 0, $width, $width, $transparency);
        //imagecopyresampled($this->dest_image, $wm, $dest_x, $dest_y, 0, 0, $width, $height, $width, $height);

        $this->modified = true;
    }

	function getImageHandler()
	{
		return $this->image;
	}


	function outputImage($method, $location = null)
	{
		// make sure they put an image filename
		if($location != null && $location{(strlen($location) - 1)} == '/')
		{
			$location .= 'newimage';
		}

		// Copy over source file if no action required (nothing changed)
		if (!empty($location) && $this->extension == $method && !$this->modified)
		{
			copy($this->filename, $location);
		}
		// Save or output file
		else
		{
			$method = strtolower($method);

			switch($method)
			{
				case 'png':
					imagealphablending($this->dest_image, false);
					imagesavealpha($this->dest_image, true);
					imagepng($this->dest_image, $location);
					break;

				case 'gif':
					imagegif($this->dest_image, $location);
					break;

				case 'bmp':
					imagejpeg($this->dest_image, $location, 90);
					$this->extension = 'jpg';
					break;

				default: //jpg, jpeg and others
					imagejpeg($this->dest_image, $location, 100);
			}
		}

		if ($location)
		{
			chmod($location, 0777);
		}
	}


	/**
	 * Save as current format
	 *
	 * @param string $location
	 */
	function save($location)
	{
		$this->outputImage($this->extension, $location);
	}

	/**
	 * Save as PNG
	 *
	 * @param string $location
	 */
	function savePNG($location)
	{
		$this->outputImage('png', $location);
	}

	/**
	 * Save as JPG
	 *
	 * @param string $location
	 */
	function saveJPG($location)
	{
		$this->outputImage('jpg', $location);
	}

	/**
	 * Save as GIF
	 *
	 * @param string $location
	 */
	function saveGIF($location)
	{
		$this->outputImage('gif', $location);
	}

	/**
	 * Show as PNG
	 *
	 */
	function outputPNG()
	{
		$this->outputImage('png');
	}

	/**
	 * Show as JPG
	 *
	 */
	function outputJPG()
	{
		$this->outputImage('jpg');
	}

	/**
	 * Show as GIF
	 *
	 */
	function outputGIF()
	{
		$this->outputImage('gif');
	}

	/**
	 * Show as current format
	 *
	 */
	function output()
	{
		$this->outputImage($this->extension);
	}
}


/*********************************************/
/* Fonction: ImageCreateFromBMP              */
/* Author:   DHKold                          */
/* Contact:  admin@dhkold.com                */
/* Date:     The 15th of June 2005           */
/* Version:  2.0B                            */
/*********************************************/
// 01/21/2009 Added support of 32 bits images (Stan Misiurev)

function imagecreatefrombmp($filename)
{
	//Ouverture du fichier en mode binaire
	$f1 = fopen($filename, 'rb');
	if (!$f1) return false;

	$FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
	if ($FILE['file_type'] != 19778) return false;

	//2 : Chargement des ent?tes BMP
	$BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
				  '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
				  '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
	$BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
	if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
	$BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
	$BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
	$BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
	$BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
	$BMP['decal'] = 4-(4*$BMP['decal']);
	if ($BMP['decal'] == 4) $BMP['decal'] = 0;

	//3 : Chargement des couleurs de la palette
	$PALETTE = array();
	if ($BMP['colors'] < 16777216 && $BMP['colors'] != 65536)
	{
		$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
		#nei file a 16bit manca la palette,
	}

	//4 : Creation de l'image
	$IMG = fread($f1,$BMP['size_bitmap']);
	$VIDE = chr(0);

	$res = imagecreatetruecolor($BMP['width'],$BMP['height']);
	$P = 0;
	$Y = $BMP['height']-1;
	while ($Y >= 0)
	{
		$X=0;
		while ($X < $BMP['width'])
		{
			// 32 bits
			if ($BMP['bits_per_pixel'] == 32)
			{
				$COLOR = unpack("V",substr($IMG,$P,4).$VIDE);
			}
			// 24 bits
			elseif ($BMP['bits_per_pixel'] == 24)
			{
				$COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
			}
			// 16 bits
			elseif ($BMP['bits_per_pixel'] == 16)
			{
				$COLOR = unpack("v",substr($IMG,$P,2));
				$blue  = (($COLOR[1] & 0x001f) << 3) + 7;
				$green = (($COLOR[1] & 0x03e0) >> 2) + 7;
				$red   = (($COLOR[1] & 0xfc00) >> 7) + 7;
				$COLOR[1] = $red * 65536 + $green * 256 + $blue;
			}
			// 8 bits
			elseif ($BMP['bits_per_pixel'] == 8)
			{
				$COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			// 4 bits
			elseif ($BMP['bits_per_pixel'] == 4)
			{
				$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
				if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			// 1 bit
			elseif ($BMP['bits_per_pixel'] == 1)
			{
				$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
				if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
				elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
				elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
				elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
				elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
				elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
				elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
				elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			// Unknown depth
			else
			{
		        return false;
			}

			imagesetpixel($res,$X,$Y,$COLOR[1]);
			$X++;
			$P += $BMP['bytes_per_pixel'];
		}
		$Y--;
		$P+=$BMP['decal'];
	}

	// Close file
	fclose($f1);

	return $res;
}
?>
