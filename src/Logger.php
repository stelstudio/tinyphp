<?php
namespace Tiny;

use Monolog\ErrorHandler;

/**
 * Wrapper for monolog
 */
class Logger
{
    private $logger;

    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public static function instance($name = 'main')
    {
        static $instance = array();
        if (!array_key_exists($name, $instance)) {
            $instance[$name] = new static($name);
        }

        return $instance[$name];
    }


    protected function __construct($name)
    {
        $this->logger = new \Monolog\Logger($name);

        // Get handlers from settings
        $handlers = Settings::get('loggers', array());

        foreach ($handlers as $handler)
        {
            $this->logger->pushHandler($handler);
        }

        if ($name == 'main')
        {
            ErrorHandler::register($this->logger);
        }
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function log($str)
    {
        // Convert arrays to string
        if (is_array($str))
        {
            $str = json_encode($str);
        }
        $this->logger->addInfo($str);
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array(array($this->logger, $name), $arguments);
    }
}