<?php
namespace Tiny\Controller;

use Tiny\User\AdminLogin;
use Tiny\User\ManagerLogin;
use Tiny\User\MustLogin;

/**
 * Base class for all controllers
 */
class Base
{
    // Anyone has access
	const ACCESS_ANYONE = 0;
    // Must be logged into the system
	const ACCESS_AUTHORIZED = 1;
    // Must be logged into the system as manager
    const ACCESS_MANAGER = 2;
    // Must be logged into the system as admin
    const ACCESS_ADMIN = 3;

	protected $access_type = self::ACCESS_ANYONE;

    public function accessType()
    {
        return $this->access_type;
    }

	/**
	 * Checks if user has access to controller and particular method
	 */
	public function access($method)
	{
		if ($this->access_type == self::ACCESS_ANYONE)
		{
			return true;
		}

        $ml = null;

        if ($this->access_type == self::ACCESS_MANAGER)
        {
            $ml = new ManagerLogin();
        }
        elseif ($this->access_type == self::ACCESS_ADMIN)
        {
            $ml = new AdminLogin();
        }
        else
        {
            $ml = new MustLogin();
        }

		return $ml->has_access(get_class($this), $method);
	}
}