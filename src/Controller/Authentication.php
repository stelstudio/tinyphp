<?php
namespace Tiny\Controller;

use OAuth\Common\Consumer\Credentials;
use OAuth\Common\Http\Uri\UriFactory;
use OAuth\Common\Storage\Memory;
use OAuth\ServiceFactory;
use Tiny\Response\HTML;
use Tiny\Settings;
use Tiny\Template;
use Tiny\User\CurrentUser;


abstract class Authentication extends Base
{
    private $methods;
    private $oauth_uri;
    private $oauth_services = array();
    private $local_table;
    protected $main_template = 'main';
    protected $main_title = 'Authentication';

    function __construct()
    {
        $settings = Settings::get('authentication');
        $this->methods = $settings['methods'];

        // RAW authentication enabled
        if (array_key_exists('local', $this->methods))
        {
            $this->enableLocal($this->methods['local']);
            unset($this->methods['local']);
        }
        // Other methods (OAuth)
        if (count($this->methods))
        {
            $this->enableOauth($this->methods);
        }
    }

    /**
     * Entry point
     */
    function __default()
    {
        // Logout user
        if ($_REQUEST['logout'])
        {
            $this->logout();
            $this->redirectNext();
            return;
        }

        // Check if already authenticated
        if ($this->isAuthenticated())
        {
            $this->redirectNext();
            return;
        }

        // Process username/password authentication
        if (isset($_POST['username']) && isset($_POST['password']))
        {
            if ($this->localLogin($_POST['username'], $_POST['password']))
            {
                $this->redirectNext();
                return;
            }
        }

        // Process OAuth response
        if ($_REQUEST['oauth_proceed'])
        {
            if ($this->oauthProceed())
            {
                $this->redirectNext();
                return;
            }
        }

        $this->loginForm();
    }

    /**
     * Authorize user in system
     * @param $method - authentication method
     * @param $user
     * @return bool
     */
    abstract protected function login($method, $user);

    /**
     * Logout user.
     */
    protected function logout()
    {
        CurrentUser::logout();
    }

    /**
     * Check if user is authenticated
     * @return bool
     */
    protected function isAuthenticated()
    {
        return !CurrentUser::anonymous();
    }

    /**
     * Redirect to next page
     */
    protected function redirectNext()
    {
        $url = ($_REQUEST['back_url'])  ?  $_REQUEST['back_url']  :  '/';
        header("Location: $url");
        exit(0);
    }

    protected function localLogin($username, $password)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * Process data passed by OAuth service and try to authenticate user
     * @return bool
     */
    protected function oauthProceed()
    {
        if (empty($_GET['code'])) {
            return false;
        }

        $oauth   = $_REQUEST['oauth_proceed'];
        $service = $this->oauth_services[$oauth];
        $user    = array();

        try
        {
            if (!$service->requestAccessToken($_GET['code']))
            {
                return false;
            }
        }
        catch (\Exception $e)
        {
            $this->redirectNext();
        }


        if ($oauth == 'facebook')
        {
            $result = json_decode($service->request('/me'), true);

            $user['oauth_id']   = $result['id'];
            $user['email']      = $result['email'];
            $user['login']      = $result['username'];
            $user['first_name'] = $result['first_name'];
            $user['last_name']  = $result['last_name'];
        }

        if (!$user['oauth_id'])
        {
            return false;
        }

        return $this->login($oauth, $user);
    }

    /**
     * Render login page
     */
    protected function loginForm()
    {
        $context = array('oauth' => array(), 'back_url' => $_REQUEST['back_url']);
        foreach ($this->oauth_services as $name => $service)
        {
            $context['oauth'][$name] = ''.$service->getAuthorizationUri();
        }

        $form = Template::load('authentication/login_form.tpl', $context);

        $page = new HTML($this->main_template, $this->main_title);
        $page->render($form);
    }

    /**
     * Turn local authentication on
     * @param $params - array
     */
    protected function enableLocal($params)
    {
        $this->local_table = $params['table'];
    }

    /**
     * Turn OAuth on and initialize all supported services
     * @param $methods
     */
    protected function enableOauth($methods)
    {
        $uriFactory = new UriFactory();
        $this->oauth_uri = $uriFactory->createFromSuperGlobalArray($_SERVER);

        foreach ($methods as $method => $params)
        {
            $this->oauth_services[$method] = $this->initOauthService($method, $params['key'], $params['secret'], $params['scope']);
        }
    }

    /**
     * Initialize OAuth service
     * @param $name - service name (facebook, google and etc.)
     * @param $key  - application id
     * @param $secret - secret key
     * @return \OAuth\Common\Service\ServiceInterface
     */
    protected function initOauthService($name, $key, $secret, $scope)
    {
        static $serviceFactory;
        static $storage;

        if (is_null($serviceFactory))
        {
            $serviceFactory = new ServiceFactory();
        }

        if (is_null($storage))
        {
            $storage = new Memory();
        }

        $this->oauth_uri->setQuery('oauth_proceed='.$name);

        if ($_REQUEST['back_url'])
        {
            $this->oauth_uri->addToQuery('back_url', $_REQUEST['back_url']);
        }

        // Setup the credentials for the requests
        $credentials = new Credentials($key, $secret, $this->oauth_uri->getAbsoluteUri());

        return $serviceFactory->createService($name, $credentials, $storage, $scope);
    }

} 