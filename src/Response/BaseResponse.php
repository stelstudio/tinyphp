<?php
namespace Tiny\Response;

use Tiny\Message;
use Tiny\Template;
use Tiny\Logger;

abstract class BaseResponse
{
    protected $headers = array();
    public $encoding = 'utf-8';
    public $content_type = 'text/html';


    function addHeader($string, $replace = true, $http_response_code = null)
    {
        $this->headers[] = compact('string', 'replace', 'http_response_code');
    }

    function sendHeaders()
    {
        if (headers_sent($filename, $linenum))
        {
            Logger::instance()->error("Headers already sent in $filename on line $linenum");
            return;
        }

        header("Content-type: $this->content_type; charset=$this->encoding");

        foreach ($this->headers as $header)
        {
            header($header['string'], $header['replace'], $header['http_response_code']);
        }
    }

    abstract function render($content = null);
}