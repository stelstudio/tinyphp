<?php
namespace Tiny\Response;


class JSON extends BaseResponse
{
    public $content_type = 'application/json';
    const JSONP_CONTENT_TYPE = 'application/javascript';

    protected $headers = array();

    function __construct()
    {

    }


    function render($data = null)
    {
        $this->sendHeaders();

        echo json_encode($data);
    }
} 