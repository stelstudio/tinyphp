<?php
namespace Tiny\Response;

use Tiny\Message;
use Tiny\Template;
use Tiny\Logger;

class HTML extends BaseResponse
{
    public $title = '';
    public $keywords = '';
    public $description = '';

    protected $template;
    protected $_content;
    protected $_css = array();

    protected static $blocks = array();

    /**
     * Register content block
     * @param $name
     * @param $content
     */
    static function registerBlock($name, $content)
    {
        self::$blocks[$name] = $content;
    }

    function __construct($template, $title)
    {
        Message::registerToPage();

        $this->template = $template;
        $this->title    = $title;
    }

    /**
     * Render page
     * @param $content - anonymous function or string
     */
    function render($content = null)
    {
        $this->sendHeaders();

        if ($content)
            $this->_content = $content;

        Template::render($this->template, array('page' => $this));

    }

    /**
     * Output content of a block or execute callback
     */
    function block($name)
    {
        $cont = self::$blocks[$name];
        if (is_string($cont))
        {
            return $cont;
        }

//        if(is_callable(array($this, $method))) {
//            return call_user_func_array($this->$method, $args);
//        }

        if (is_callable($cont))
        {
            return $cont->__invoke();
        }
    }

    /**
     * Primary content of a page
     * @param $content
     */
    function content()
    {
        if (is_string($this->_content))
        {
            return $this->_content;
        }

        if (is_callable($this->_content))
        {
            return $this->_content->__invoke();
        }
    }


    function addCSS($path)
    {
        $this->_css[] = $path;
    }

    function css()
    {
        $result = '';
        foreach (array_unique($this->_css) as $path)
        {
            $result .= '<link rel="stylesheet" href="'.$path.'"/>';
        }
        return $result;
    }


    function __get($name)
    {
        if ($name == 'content')
        {
            echo $this->content();
        }
        elseif ($name == 'css')
        {
            echo $this->css();
        }
        elseif (array_key_exists($name, self::$blocks))
        {
            echo $this->block($name);
        }
    }

    /**
     * Set content of a page, .render() is preferrable
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->_content = $content;
    }

}