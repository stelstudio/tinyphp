<?php
namespace Tiny\Form;

use Tiny\Form\Element\BaseElement;
use Tiny\Form\Errors;
use Tiny\Form\Formatter\FormatterTrait;


abstract class BaseForm implements \ArrayAccess
{
    use FormatterTrait;

    protected $initial_data = array();
    protected $_elements;
    protected $_attr;
    private $default_attrs = array(
        'method' => 'post'
    );
    protected $_errors;
    protected $_buttons = null;
    /**
     * @var string
     */
    private $formatter;

    function __construct($initial_data = array(), $formatter = '\Tiny\Form\Formatter\Bootstrap')
    {
        $frm = new $formatter();

        $this->formatter($frm);

        $this->_attr = new Attributes(array_merge(
            $this->default_attrs,
            $this->attributes()
        ));
        $this->formatter = $formatter;
        $this->initial_data = $initial_data;
    }

    function __sleep()
    {
        return array('_elements', '_attr', '_errors');
    }

    function __wakeup()
    {
        //
    }

    /**
     * Override to specify attributes of the form
     * @return array
     */
    function attributes()
    {
        return array();
    }

    function isValid($data)
    {
        // Reset errors
        $this->_errors = new Errors();
        $this->_errors->formatter($this->formatter());

        // Check each element
        foreach ($this->_elements as $name => $element)
        {
            $error_code = null;
            $value = $data[$name];
            if (is_null($value))
            {
                // Value should not be null once form submitted
                $value = '';
            }            

            $element->value($value);
            if (!$element->isValid($error_code))
            {
                $this->_errors[$name] = $element->label().' is '.$element->error();
            }
        }

        // Valid only if there are no errors
        return !(count($this->_errors));
    }

    /**
     * Save form data
     */
    abstract function save($data);


    function attr($name, $value)
    {
        $this->_attr[$name] = $value;
        return $this;
    }

    /**
     * Output form markup
     */
    function render()
    {
        echo $this->__toString();
    }


    function __toString()
    {
    	$html = $this->openTag();
        foreach ($this->_elements as $i => $el)
        {
            $html .= $this->getElement($i);
        }
        $html .= $this->buttonsBar();
        $html .= $this->closeTag();
        return $html;
    }

    function __get($name)
    {
		if ($name == 'open_tag')
		{
			return $this->openTag();
		}

		if ($name == 'close_tag')
		{
			return $this->closeTag();
		}

        if ($name == 'buttons_bar')
        {
            return $this->buttonsBar();
        }
    }

    function openTag()
    {
        $enctype = '';
        foreach ($this->_elements as $name => $element) 
        {
            if ($element instanceof \Tiny\Form\Element\File)
            {
                $enctype = ' enctype="multipart/form-data"';
                break;
            }
        }
        
		return '<form '.$this->_attr.$enctype.'>';
    }

    function closeTag()
    {
    	return '</form>';
    }

    function buttonsBar()
    {
        $formatter = $this->formatter();
        $buttons = ($this->_buttons)  ?  $this->_buttons  :  $this->defaultButtons();
        return $buttons->formatter($formatter);
    }

    function offsetSet($offset, $value) {
        if (is_null($offset))
        {
            throw Exception("Specify element name as a key");
        }

        if (!($value instanceof BaseElement))
        {
            throw Exception("You are trying to assign invalid element to the form");
        }

    	$value->attr('name', $offset);
        $this->_elements[$offset] = $value;
    }

    function offsetExists($offset) {
        return isset($this->_elements[$offset]);
    }

    function offsetUnset($offset) {
        unset($this->_elements[$offset]);
    }

    function offsetGet($offset)
    {
        return $this->getElement($offset);
    }

    function getElement($offset)
    {
        if (isset($this->_elements[$offset]))
        {
            $formatter = $this->formatter();
            $el = $this->_elements[$offset];
            $el->formatter($formatter);
            // Pass initial data to each element
            if (array_key_exists($offset, $this->initial_data) && is_null($el->defaultValue()))
            {
                $el->defaultValue($this->initial_data[$offset]);
            }
            return $el;
        }

        return null;
    }

    /**
     * Get list of errors
     * @return array
     */
    function errors() {
        return $this->_errors;
    }

    /**
     * Check if there are errors in the form
     * @return bool
     */
    function hasErrors() {
        return (count($this->_errors) > 0);
    }

    function buttons(ButtonsBar $buttons = null)
    {
        // Getter
        if (is_null($buttons))
        {
            return $this->_buttons;
        }

        // Setter
        $this->_buttons = $buttons;
        return $this;
    }

    function defaultButtons()
    {
        $bar = new ButtonsBar();
        $bar[] = new Attributes(array(
            'type' => 'submit',
            'value' => 'Submit'
        ));

        return $bar;
    }
} 
