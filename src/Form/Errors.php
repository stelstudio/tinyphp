<?php
/**
 * Created by PhpStorm.
 * User: Stan
 * Date: 7/3/15
 * Time: 5:59 PM
 */

namespace Tiny\Form;

use Tiny\Form\Formatter\FormatterTrait;

/**
 * Class Errors
 * @package Tiny\Form
 * Container for validation errors that can be output as HTML code
 */
class Errors extends \ArrayObject
{
    use FormatterTrait;

    function __construct($list = [])
    {
        parent::__construct($list, self::STD_PROP_LIST);
    }

    function __toString()
    {
        return $this->_formatter->formatErrors($this);
    }


} 