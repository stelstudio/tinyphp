<?php

namespace Tiny\Form;

use Tiny\Form\Formatter\FormatterInterface;
use Tiny\Form\Attributes;
use Tiny\Form\Formatter\FormatterTrait;


class ButtonsBar extends \ArrayObject {
    use FormatterTrait;

    function __construct($buttons = [])
    {
        parent::__construct($buttons, self::STD_PROP_LIST);
    }

    /**
     * Add a button
     * @param $caption
     * @param null $name
     * @param null $id
     * @param null $onclick
     * @param string $type
     */
    function addButton($caption, $name = null, $id = null, $type = 'button')
    {
        $value = $caption;
        parent::append(new Attributes(compact('value', 'id', 'name', 'type')));
        return $this;
    }

    /**
     * Add submit button
     * @param $caption
     * @param null $name
     * @param null $id
     * @param null $onclick
     */
    function addSubmitButton($caption, $name = null, $id = null)
    {
        return $this->addButton($caption, $name, $id, 'submit');
    }

    function addResetButton($caption)
    {
        return $this->addButton($caption, null, null, 'submit');
    }

    function addLink($caption, $id, $href)
    {
        $type = 'link';
        parent::append(new Attributes(compact('type', 'caption', 'id', 'href')));
        return $this;
    }


    function __toString()
    {
        return $this->formatter()->formatButtonBar($this);
    }
} 