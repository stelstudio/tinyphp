<?php
/**
 * Created by PhpStorm.
 * User: Stan
 * Date: 7/2/15
 * Time: 9:00 PM
 */

namespace Tiny\Form\Formatter;


class Bootstrap implements FormatterInterface
{
    public function formatErrors(array $errors)
    {
        if (!count($errors))
        {
            return '';
        }

        $list = '';
        foreach ($errors as $err)
        {
            $list .= '<li>'.$err.'</li>';
        }

        return <<<ERRORS_BLOCK
<ul class="bg-danger form-errors">
    $list
</ul>
ERRORS_BLOCK;
    }

    public function formatRow($id, $label, $input, $help_text = '', $error = '')
    {
        $label_tag = ($label)  ?  '<label for="'.$id.'">'.$label.'</label>'  :  '';
        $help_tag = ($help_text)  ?  '<p class="help-block">'.$help_text.'</p>'  :  '';
        $class_name = 'form-group';
        if ($error)
        {
            $class_name .= ' has-error has-feedback';
        }

        return <<<HTML_ROW
<div class="$class_name">
    $label_tag
    $input
    $help_tag
</div>

HTML_ROW;

    }

    public function formatButtonBar($buttons)
    {
        $list = '';
        foreach ($buttons as $btn)
        {
            if (!$btn['type'])
            {
                $btn['type'] = 'button';
            }

            // Add .btn
            $btn['class'] = ($btn['class'])  ?  "$btn[class] btn"  :  'btn';

            // Make submit button to stand out
            $btn['class'] .= ($btn['type'] == 'submit')  ?  ' btn-primary'  :  '';

            if ($btn['type'] == 'link')
            {
                $caption = $btn['caption'];
                unset($btn['caption']);

                $btn['class'] .= ' btn-link';

                $list .= "<a $btn>$caption</a> ";
            }
            else
            {
                $list .= "<input $btn/> ";
            }
        }

        return <<<FORM_ACTIONS
<div class="form-actions">
    $list
</div>
FORM_ACTIONS;

    }

} 