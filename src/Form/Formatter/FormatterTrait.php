<?php
namespace Tiny\Form\Formatter;

/**
 * Trait FormatterTrait
 * @package Tiny\Form\Formatter
 * Getter/setter for "formatter" property
 */
trait FormatterTrait
{
    protected $_formatter = null;

    /**
     * Get/set formatter
     * @param $formatter
     */
    function formatter($formatter = null)
    {
        // Getter
        if (is_null($formatter))
        {
            if (is_string($this->_formatter))
            {
                try
                {
                    $this->_formatter = new $this->_formatter();
                }
                catch (\Exception $e)
                {
                    throw \Exception("Cannot initialize formatter. Does class $this->_formatter exist?");
                }
            }

            return $this->_formatter;
        }

        // Setter
        $this->_formatter = $formatter;
        return $this;
    }
} 