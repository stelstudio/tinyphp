<?php
namespace Tiny\Form\Formatter;

interface FormatterInterface
{
    /**
     * Generate markup for errors block
     * @param array $errors
     * @return mixed
     */
    public function formatErrors(array $errors);

    /**
     * Generate markup for each row
     * @param $id
     * @param $label
     * @param $input
     * @param $help_text
     * @param $error
     * @return mixed
     */
    public function formatRow($id, $label, $input, $help_text, $error);

    /**
     * Format bar with buttons
     * @param $buttons
     * @return mixed
     */
    public function formatButtonBar($buttons);
}
