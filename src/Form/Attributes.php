<?php
/**
 * Created by PhpStorm.
 * User: Stan
 * Date: 4/26/15
 * Time: 8:30 PM
 */

namespace Tiny\Form;

/**
 * Class Attributes
 * @package Tiny\Form
 */
class Attributes implements \ArrayAccess
{
    protected $attr = array();

    function __construct($initial = array())
    {
        if (!is_array($initial))
        {
            $this->attr = array();
        }
        else 
        {
            foreach ($initial as $key => $value)
            {
                $this->attr[$key] = $value;
            }
        }
    }

    function __sleep()
    {
        return array('attr');
    }

    /**
     * Add class name
     * @param $name string
     */
    function addClass($name)
    {
        $this->append('class', $name);
    }

    /**
     * Delete class name
     * @param $name string
     * @return bool - true if class was found and removed
     */
    function removeClass($name)
    {
        $class_names = $this->offsetGet('class');
        if (!$class_names)
        {
            return false;
        }

        $classes = explode(' ', $class_names);
        if (!in_array($name, $classes))
        {
            return false;
        }

        $this->offsetSet('class', implode(' ', array_diff($classes, array($name))));
        return true;
    }

    /**
     * Add inline style
     */
    function addStyle($name)
    {
        $this->append('style', $name, ';');
    }

    /**
     * Append to attribute, e.g. class
     */
    function append($attr_name, $str, $separator = ' ')
    {
        // Simplest case when attribute is not set yet
        if (!$this->offsetExists($attr_name))
        {
            $this->attr[$attr_name] = $str;
        }

        $values = explode($separator, $this->attr[$attr_name]);
        if (in_array($str, $values))
        {
            // Already there
            return;
        }

        $this->attr[$attr_name] = trim($this->attr[$attr_name]).$separator.$str;
    }

    function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->attr[] = $value;
        } else {
            $this->attr[$offset] = $value;
        }
    }

    function offsetExists($offset) {
        return array_key_exists($offset, $this->attr);
    }

    function offsetUnset($offset) {
        unset($this->attr[$offset]);
    }

    function offsetGet($offset) {
        return isset($this->attr[$offset]) ? $this->attr[$offset] : null;
    }

    function __toString()
    {
        return implode(' ', array_map(function ($v, $k) {
            // Skip attributes with empty value
            if (!$k) return '';

            return $k.'="'.htmlspecialchars($v).'"';
        }, $this->attr, array_keys($this->attr)));
    }
} 