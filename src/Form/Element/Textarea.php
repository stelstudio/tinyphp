<?php
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;

class Textarea extends BaseElement 
{
    function __construct($attributes = array())
    {
        // Append form-control class name
        $attributes['class'] = (array_key_exists('class', $attributes))  ?  "$attributes[class] form-control"  :  'form-control';

        $this->_attr = new Attributes($attributes);
    }

    function __toString()
    {
        return $this->_formatter->formatRow(
            $this->id(),
            $this->_label,
            "<textarea $this->_attr>".htmlspecialchars($this->_value).'</textarea>',
            $this->_help,
            $this->_error
        );
    }

} 