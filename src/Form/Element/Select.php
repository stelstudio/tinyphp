<?php
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;

class Select extends BaseElement 
{

    /**
     * Flag that indicates if empty option should be rendered
     * @var bool
     */
    protected $_empty;

    /**
     * List of options
     * @var array
     */
    protected $_options;

    function __construct($attributes = array())
    {
        // Append form-control class name
        $attributes['class'] = (array_key_exists('class', $attributes))  ?  "$attributes[class] form-control"  :  'form-control';

        $this->_attr = new Attributes($attributes);
    }

    function options($array, $empty=null)
    {
        if (!is_null($empty))
        {
            $this->_empty = $empty;
        }
        $this->_options = $array;
        return $this;
    }

    /**
     * Get / set "multiple" attribute
     * @param null $bool
     */
    function multiple($bool = null)
    {
        // Getter
        if (is_null($bool))
        {
            return ($this->_attr['multiple']);
        }

        // Setter
        if ($bool)
        {
            $this->_attr['multiple'] = 'multiple';
        }
        else
        {
            unset($this->_attr['multiple']);
        }
    }


    function renderOptions($array)
    {
        $html = '';
        foreach ($array as $key => $value) {
            if (is_array($value))
            {
                $html .= '<optgroup label="'.htmlspecialchars($key).'">'.$this->renderOptions($value).'</optgroup>';
            }
            else
            {
                $selected = '';
                // Multiple options selected
                if (is_array($this->value()) && in_array($key, $this->value()))
                {
                    $selected = ' selected="selected"';
                }
                // Option selected
                elseif ($key == $this->value())
                {
                    $selected = ' selected="selected"';
                }
                $html .= '<option value="'.htmlspecialchars($key).'"'.$selected.'>'.htmlspecialchars($value).'</option>';
            }
        }
        return $html;
    }

    function __toString()
    {
        // Generate id
        $id = $this->id();

        // Selects with "multiple" should have [] at the end of name
        $name = $this->_attr['name'];
        if ($this->_attr['multiple'] && substr($name, -2))
        {
            $this->_attr['name'] = $name.'[]';
        }

        $options = '';
        if ($this->_empty)
        {
            $options .= '<option value="">'.htmlspecialchars($this->_default).'</option>';
        }
        $options .= $this->renderOptions($this->_options);

        return $this->_formatter->formatRow(
            $id,
            $this->_label,
            "<select $this->_attr>".$options.'</select>',
            $this->_help,
            $this->_error
        );
    }

} 