<?php
namespace Tiny\Form\Element;


class Hidden extends Input {

    function __construct($attributes = array())
    {
        $attributes['type'] = 'hidden';
        parent::__construct($attributes);
    }

    function __toString()
    {
        return $this->input_box();
    }
} 