<?php
namespace Tiny\Form\Element;

class Image extends File {

    function uploadedHtml()
    {
        $img = new \Tiny\Image($this->upload_to.'/'.$this->uploaded_file);

        $html = '<div class="uploaded-image">';
        $html .=   '<input type="hidden" name="'.$this->name().'_uploaded" value="'.htmlspecialchars($this->uploaded_file).'"/>';
        $html .=   '<input type="hidden" name="'.$this->name().'_uploadedname" value="'.htmlspecialchars($this->uploaded_name).'"/>';
        $html .=   '<img src="'.$img.'"/>';
        $html .=   '<a class="uploaded-file-delete" href="#" title="Delete"><i class="fa fa-trash-o"></i></a>';
        $html .= '</div>';
        return $html;
    }
} 