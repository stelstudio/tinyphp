<?php
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;

class Checkbox extends BaseElement {

    function __construct($attributes = array())
    {
        $this->_attr = new Attributes($attributes);
        $this->_attr['type'] = 'checkbox';
    }

    function __toString()
    {
        $this->id();
        $value = $this->value();
        if (is_null($value))
        {
        	$value = $this->defaultValue();
        }
        
        $checked = ($value)  ?  'checked="checked"'  :  '';
        return $this->_formatter->formatRow(
            $this->id(),
            '',
            "<label><input $this->_attr $checked value='on'/> $this->_label</label>",
            $this->_help,
            $this->_error
        );
    }

} 