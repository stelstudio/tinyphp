<?php
namespace Tiny\Form\Element;


class Email extends Input
{
    const EMAIL_INVALID = 'email_invalid';

    function __construct($attributes = array())
    {
        $attributes['type'] = 'email';
        parent::__construct($attributes);
    }

    /**
     * Check if email is correct
     * @param $error_code
     * @return bool
     */
    function isValid()
    {
        if (!parent::isValid())
        {
            return false;
        }

        // Empty values are valid unless required
        if (!$this->value())
        {
            return true;
        }

        $val = filter_var($this->value(), FILTER_SANITIZE_EMAIL);
        if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
            $this->_error = self::EMAIL_INVALID;
            return false;
        }

        $this->value($val);
        return true;
    }
} 