<?php
namespace Tiny\Form\Element;


class Password extends Input
{
    function __construct($attributes = array())
    {
        $attributes['type'] = 'password';
        parent::__construct($attributes);
    }
    /**
     * @TODO add validation methods to check for complexity
     */
} 