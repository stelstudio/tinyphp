<?php
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;

class Input extends BaseElement {

    function __construct($attributes = array())
    {
        // Append form-control class name
        $attributes['class'] = (array_key_exists('class', $attributes))  ?  "$attributes[class] form-control"  :  'form-control';

        $this->_attr = new Attributes($attributes);
    }

    protected function input_box()
    {
        $value = '';
        if ($this->_value)
        {
            $value = htmlspecialchars($this->_value);
        }
        elseif ($this->_default)
        {
            $value = htmlspecialchars($this->_default);
        }

        return "<input $this->_attr value=\"$value\"/>";
    }

    function __toString()
    {
        return $this->_formatter->formatRow(
            $this->id(),
            $this->_label,
            $this->input_box(),
            $this->_help,
            $this->_error
        );
    }

} 