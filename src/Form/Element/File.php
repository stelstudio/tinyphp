<?php
namespace Tiny\Form\Element;


class File extends Input {
    protected $upload_to;
    protected $uploaded_file;
    protected $uploaded_name;

    function __construct($upload_to)
    {
        parent::__construct();
        $this->attr('type', 'file');
        $this->attr('class', '');

        $this->upload_to = $upload_to;
    }

    function value($str = null)
    {
        // Nothing has been uploaded
        if (!array_key_exists($this->name(), $_FILES))
        {
            return $this;
        }

    	$file = $_FILES[$this->name()];
    	// New file uploaded
        if ($file && !$file['error'])
        {
            $this->processUpload($file);
            return $this;
        }

    	// @TODO decide how to get data from $data instead of $_POST
    	// Check if file has been uploaded before
        if ($_POST[$this->name().'_uploaded'] && $_POST[$this->name().'_uploadedname'])
        {
        	$this->uploaded_file = $_POST[$this->name().'_uploaded'];
			$this->uploaded_name = $_POST[$this->name().'_uploadedname'];

			return $this;
        }

        return $this;
    }

    /**
     * Get path to uploaded file
     * @return string
     */
    function getFile()
    {
        if (!$this->uploaded_file)
        {
            return null;
        }

        return $this->upload_to.'/'.$this->uploaded_file;
    }

    /**
     * Move uploaded file to persistent location
     * @param $file
     */
    protected function processUpload($file)
    {
        $filename = $this->randomName();
        if (move_uploaded_file($file['tmp_name'], $this->upload_to.'/'.$filename))
        {
            $this->uploaded_file = $filename;
            $this->uploaded_name = $file['name'];
        }
    }

    /**
     * Generate random, unique name for a file
     * @return string
     */
    protected function randomName()
    {
        $prefix = $this->name().'_';
        $filename = '';
        do
        {
            $filename = uniqid($prefix);
        }
        while (file_exists($this->upload_to.'/'.$filename));

        return $filename;
    }

    function __toString()
    {
        // File input should never have value set
        $this->_value = '';

        $html = '';

        if ($this->uploaded_file)
        {
            $html .= $this->uploadedHtml();
        }

        return $this->_formatter->formatRow(
            $this->id(),
            $this->_label,
            $html."<input $this->_attr/>",
            $this->_help,
            $this->_error
        );
    }

    function uploadedHtml()
    {
        $html = '<input type="hidden" name="'.$this->name().'_uploaded" value="'.htmlspecialchars($this->uploaded_file).'"/>';
        $html .= '<input type="hidden" name="'.$this->name().'_uploadedname" value="'.htmlspecialchars($this->uploaded_name).'"/>';
        $html .= '<div class="uploaded_file">';
        $html .= $this->uploaded_name;
        $html .= '<a href="#" title="Delete"><i class="fa fa-trash-o"></i></a>';
        $html .= '</div>';
        return $html;
    }
} 