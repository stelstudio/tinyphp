<?php
namespace Tiny\Form\Element;


class HTML extends BaseElement {
    protected $_html;

    function __construct($html = '')
    {
        $this->_html = $html;
    }

    function html($str = null)
    {
        // Getter
        if (is_null($str))
        {
            return $this->_html;
        }

        $this->_html = $str;
        return $this;
    }

    function __toString()
    {
        return $this->_html;
    }

    function isValid()
    {
        return true;
    }

}