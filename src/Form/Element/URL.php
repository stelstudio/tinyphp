<?php
namespace Tiny\Form\Element;


class URL extends Input
{
    const URL_INVALID = 'url_invalid';

    function __construct($attributes = array())
    {
        $attributes['type'] = 'url';
        parent::__construct($attributes);
    }

    /**
     * Check if URL is correct
     * @param $error_code
     * @return bool
     */
    function isValid()
    {
        if (!parent::isValid())
        {
            return false;
        }

        // Empty values are valid unless required
        if (!$this->value())
        {
            return true;
        }


        $val = filter_var($this->value(), FILTER_SANITIZE_URL);
        if (!filter_var($val, FILTER_VALIDATE_URL)) {
            $this->_error = self::URL_INVALID;
            return false;
        }

        $this->value($val);
        return true;
    }
} 