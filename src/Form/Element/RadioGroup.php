<?php
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;

class RadioGroup extends Select
{
    protected $group_tag = 'h3';
    protected $_multiple = false;

    /**
     * @param string $group_tag
     */
    public function setGroupTag($group_tag)
    {
        $this->group_tag = $group_tag;
    }

    /**
     * @return string
     */
    public function getGroupTag()
    {
        return $this->group_tag;
    }

    /**
     * Get or set "multiple" flag.
     * Checkboxes will be rendered if set to True
     * @param null $bool
     * @return bool|RadioGroup
     */
    function multiple($bool = null)
    {
        // Getter
        if (is_null($bool))
        {
            return ($this->_multiple);
        }

        $this->_multiple = $bool;
        return $this;
    }

    function renderOptions($array)
    {
        $html = '';
        $type = ($this->_multiple)  ?  'checkbox'  :  'radio';

        foreach ($array as $key => $value) {
            if (is_array($value))
            {
                $title = htmlspecialchars($key);
                $html .= "<$this->group_tag>$title</$this->group_tag>";
                $html .= $this->renderOptions($value);
            }
            else
            {
                $attr = $this->_attr;
                $attr['type'] = $type;
                $attr->removeClass('form-control');

                // Multiple options selected
                if (is_array($this->value()) && in_array($key, $this->value()))
                {
                }
                // Option selected
                elseif ($key == $this->value())
                {
                    $attr['checked'] = 'checked';
                }

                $html .= '<div><input  value="'.htmlspecialchars($key).'"'.$attr.'/> '.htmlspecialchars($value).'</div>';
            }
        }
        return $html;
    }

    function __toString()
    {
        // Generate id
        $id = $this->id();

        // RadioGroup with "multiple" should have [] at the end of name
        $name = $this->_attr['name'];
        if ($this->_multiple && substr($name, -2))
        {
            $this->_attr['name'] = $name.'[]';
        }

        $options = $this->renderOptions($this->_options);

        return $this->_formatter->formatRow(
            $id,
            $this->_label,
            $options,
            $this->_help,
            $this->_error
        );
    }

}