<?
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;
use Tiny\Form\Formatter\FormatterTrait;

/**
 * Base class for all form elements
 */
abstract class BaseElement {
    use FormatterTrait;

    protected $_label;
    protected $_attr;
    protected $_value;
    protected $_help;
    protected $_error;

    /**
     * @var initial value
     */
    protected $_default;

    const VALUE_REQUIRED = 'required';

    function __construct($attributes = array())
    {
        $this->_attr = new Attributes($attributes);
    }

    function __sleep()
    {
        $props = array_keys(get_class_vars(get_class($this)));
        // Encode for safe serialization
        foreach ($props as $prop => $value)
        {
            if (!is_string($value)) continue;
            $this->$prop = base64_encode($value);
        }
        // Exclude $_help because of problems with UTF-8
        //return array_diff($props, array('_help'));
        return $props;
    }

    function __wakeup()
    {
        $props = array_keys(get_class_vars(get_class($this)));
        // Decode data encoded by __sleep()
        foreach ($props as $prop => $value)
        {
            if (!is_string($value)) continue;
            $this->$prop = base64_decode($value);
        }
    }

    /**
     * Get name
     */
    function name()
    {
        return $this->_attr['name'];
    }

    function attr($name, $value)
    {
        $this->_attr[$name] = $value;
        return $this;
    }

    function addClass($name)
    {
        $this->_attr->addClass($name);
        return $this;
    }

    function value($str = null)
    {
    	// Getter
    	if (is_null($str))
    	{
    		return $this->_value;
    	}

		// Setter
    	$this->_value = $str;
        return $this;
    }

    /**
     * Get / set default value
     */
    function defaultValue($value = null)
    {
        // Getter
        if (is_null($value))
        {
            return $this->_default;
        }

        $this->_default = $value;
        return $this;
    }
    
    /**
     * Set label
     */
    function label($str = null)
    {
    	// Getter
    	if (is_null($str))
    	{
    		return $this->_label;
    	}

    	// Setter
        $this->_label = $str;
        return $this;
    }

    /**
     * Get/set id
     */
    function id($str = null)
    {
    	// Setter
    	if (is_string($str))
    	{
			$this->_attr['id'] = $str;
			return $this;
    	}

    	// Getter

    	// Generate id
    	if (!$this->_attr['id'])
    	{
    		$this->_attr['id'] = $this->_attr['name'].'_inp';
    	}
    	
    	return $this->_attr['id'];
    }

    /**
     * Get/set help text
     */
    function helptext($str = null)
    {
    	// Getter
    	if (is_null($str))
    	{
    		return $this->_help;
    	}

    	$this->_help = $str;
    	return $this;
    }

    /**
     * Check if element value is correct
     * @param $error_code
     * @return bool
     */
    function isValid()
    {
        if ($this->_attr['required'] && !$this->value())
        {
            $this->_error = self::VALUE_REQUIRED;
            return false;
        }

        $this->_error = null;
        return true;
    }

    /**
     * Get last validation error
     */
    function error()
    {
        return $this->_error;
    }

    function setError($str)
    {
        $this->_error = $str;
        return $this;
    }

    function error_class()
    {

    }


    /**
     * Render element 
     */
	abstract function __toString();
}

?>