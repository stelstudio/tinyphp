<?php
namespace Tiny\Form\Element;

use Tiny\Form\Attributes;

/**
 * Class Captcha
 * @package Tiny\Form\Element
 *
 * Captcha for a form based on google recaptcha
 */
class Captcha extends BaseElement {

    protected $site_key;
    protected $lang;
    protected $recaptcha;


    function __construct($site_key, $secret_key, $language='en')
    {
        $this->site_key = $site_key;
        $this->lang = $language;
        $this->recaptcha = new \ReCaptcha\ReCaptcha($secret_key);
    }

    function __toString()
    {
        return '<div class="g-recaptcha" data-sitekey="'.$this->site_key.'"></div><br/>'.
        '<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl='.$this->lang.'"></script>';
    }

    function isValid()
    {
        $resp = $this->recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        return $resp->isSuccess();
    }

} 