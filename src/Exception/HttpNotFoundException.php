<?php
namespace Tiny\Exception;


class HttpNotFoundException extends HttpException
{
    function __construct($message = "Page not found", $code = 404, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}