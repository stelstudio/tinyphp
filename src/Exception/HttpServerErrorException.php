<?php
namespace Tiny\Exception;

class HttpServerErrorException extends HttpException
{
    function __construct($message = "Internal Server Error", $code = 500, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
