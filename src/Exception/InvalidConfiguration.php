<?php
namespace Tiny\Exception;

/**
 * Class InvalidConfiguration
 *
 * Raised by Settings class if configuration file is invalid
 *
 * @package Tiny
 */
class InvalidConfiguration extends \Exception { }