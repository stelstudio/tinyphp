<?php
namespace Tiny\Exception;

use Tiny\User\CurrentUser;

class HttpInvalidRequest extends HttpException
{
    function __construct($message = "Invalid request", $code = 400, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}