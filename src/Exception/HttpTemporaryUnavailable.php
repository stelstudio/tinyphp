<?php
namespace Tiny\Exception;

use Tiny\User\CurrentUser;

class HttpTemporaryUnavailable extends HttpException
{
    function __construct($message = "Service temporary unavailable", $code = 503, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}