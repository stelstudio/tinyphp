<?php
namespace Tiny\Exception;

use Tiny\User\CurrentUser;

class HttpForbiddenException extends HttpException
{
    function __construct($message = "Forbidden", $code = 403, Exception $previous = null)
    {
        // @TODO wtf??
        if ("not authenticated")
        {
            CurrentUser::logout();
            $return_url = urlencode("$_SERVER[REQUEST_URI]");
            header("Location: /auth/?back_url=$return_url");
            exit(0);
        }

        parent::__construct($message, $code, $previous);
    }
}