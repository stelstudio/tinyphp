<?php
namespace Tiny;

use Tiny\Exception\InvalidConfiguration;

class Settings
{
    private static $config;

    public static function init($path_to_config)
    {
        try {
            self::$config = include $path_to_config;
        }
        catch (Exception $e) {
            throw new InvalidConfiguration($e->getMessage());
        }

        if (!is_array(self::$config)) {
            throw new InvalidConfiguration("Invalid configuration, should be array");
        }

        Template::set_path(self::get('template_path'));
    }

    protected function __construct()
    {

    }

    public static function get($name)
    {
        return array_key_exists($name, self::$config)  ?  self::$config[$name]  :  null;
    }

    public static function __callStatic($name, $arguments)
    {
        return self::get($name);
    }

    public static function production()
    {
        return in_array(self::get('environment'), array('live', 'production'));
    }

}