<?php
namespace Tiny\User;
use Tiny\Controller\Base;

/**
 * Class ManagerLogin
 * @package Tiny
 *
 * Makes route to allow authenticated managers only
 */
class ManagerLogin implements \Tiny\Router\RouteProtection
{
    function has_access($controller = '', $method)
    {
        if (CurrentUser::anonymous())
        {
            return false;
        }

        return (CurrentUser::accessType() >= Base::ACCESS_MANAGER);
    }
}