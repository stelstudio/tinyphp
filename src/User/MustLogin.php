<?php
namespace Tiny\User;

/**
 * Class MustLogin
 * @package Tiny
 *
 * Makes route to allow authenticated users only
 */
class MustLogin implements \Tiny\Router\RouteProtection
{
    function has_access($controller = '', $method)
    {
        if (CurrentUser::anonymous())
        {
            return false;
        }

        // @TODO check permissions
        return true;
    }
}