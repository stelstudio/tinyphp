<?php
namespace Tiny\User;

class User
{
	private $id;
    private $_access_type;

	function __construct($id)
	{
		$this->id = $id;
	}

	function id()
	{
		return $this->id;
	}

    function name()
    {
        return "Stan ".$this->id();
    }

	function anonymous()
	{
		return !$this->id();
	}

    function accessType()
    {
        return $this->_access_type;
    }
}
