<?php
namespace Tiny\User;

/**
 * Static wrapper for User class. It needs to be initialized by calling init() and then it will bypass all
 * static methods to internal User object
 *
 * authenticate() and logout() methods control session variable
 */
class CurrentUser
{
    private static $user;
    private static $session_key;

    static function init($session_key, $object, $method)
    {
        self::$session_key = $session_key;
        self::$user = call_user_func(array($object, $method), $_SESSION[$session_key]);
    }

    static function authenticate($user_id)
    {
        $_SESSION[self::$session_key] = $user_id;
    }

    static function id()
    {
        return $_SESSION[self::$session_key];
    }


    static function anonymous()
    {
        return !self::id();
    }

    static function logout()
    {
        unset($_SESSION[self::$session_key]);
    }

    /**
     * Get user model
     */
    static function model()
    {
        return self::$user;
    }

    static function __callStatic($name, $arguments)
    {
        if (!self::$user)
        {
            throw new \Exception("Current user has not been initialized. Call CurrentUser::init()");
        }
        return call_user_func_array(array(self::$user, $name), $arguments);
    }
}
