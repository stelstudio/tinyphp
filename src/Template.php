<?
namespace Tiny;

class Template
{
	static public $path;
    static public $url;
    static public $frontend;
    static private $js = array();
    static private $js_inline = '';
    static private $css = array();

	static function set_path($path)
	{
		self::$path = $path;
	}
    
    static function set_url($url)
    {
        self::$url = $url;
    }
    
    static function set_frontend($path)
    {
        self::$frontend = $path;
    }

    /**
     * It can start with:
     *     - ./ - relative current path
     *     - /  - relative to document root
     *     - // - absolute, equals / in linux
     *     - others are relative to $path
     *
     *   file extension is appended automatically.
     *
     * @param $template_file
     * @return string
     */
    protected static function resolve_path($template_file)
    {
        // Use underscores to avoid context collision
        $__path__ = self::$path.'/';

        // Current path (starts with ./)
        if ($template_file[0] == '.' && $template_file[1] == '/')
        {
            $__path__ = $template_file;
        }
        // Root (starts with //)
        elseif ($template_file[0] == '/' && $template_file[1] == '/')
        {
            $__path__ = substr($template_file, 1);
        }
        // Document root
        elseif ($template_file[0] == '/' || !self::$path)
        {
            $__path__ = $_SERVER['DOCUMENT_ROOT'].'/';
        }

        if (pathinfo($template_file, PATHINFO_EXTENSION) != 'tpl')
        {
            $template_file .= '.tpl';
        }

        return $__path__.$template_file;
    }

    /**
     * Render template to a string
     * @param $template_file
     * @param null $template_data_row
     */
    static function load($template_file, $template_data_row = null)
    {
        $__path__ = self::resolve_path($template_file);
        // Prepare context
        if (is_array($template_data_row))
        {
            extract($template_data_row);
        }

        ob_start();
        include $__path__;
        $template_result = ob_get_contents();
        ob_end_clean();

        return $template_result;

    }

    /**
     * Output template
     * @param $template_file - path to template file (.tpl)
     * @param null $template_data_row - array of variables passed to a template
     * @param bool $return - return as string
     * @return mixed|string
     */
    static function render($template_file, $template_data_row = null)
	{
        $__path__ = self::resolve_path($template_file);

        // Prepare context
		if (is_array($template_data_row))
		{
			extract($template_data_row);
		}

        return include $__path__;
	}
    
    static function addJS($path)
    {
        if (is_array($path))
        {
            self::$js = array_merge(self::$js, $path);
        }
        else if ($path)
        {
            self::$js[] = $path;
        }
    }
    
    static function addInlineJS($code)
    {
        self::$js_inline .= ";\n".$code;
    }
    
    static function addCSS($path)
    {
        if (is_array($path))
        {
            self::$css = array_merge(self::$css, $path);
        }
        else if ($path)
        {
            self::$css[] = $path;
        }
    }
    
    static function js()
    {
        foreach(array_unique(self::$js) as $path)
        {
            // Check if javascript is external (starts with //)
            if (substr($path, 0, 3) != '//')
            {
                $path = self::$frontend.$path;
            }
            echo '<script src="'.$path.'" type="text/javascript"></script>';
        }
        
        if (self::$js_inline)
        {
            echo '<script type="text/javascript">'.self::$js_inline.'</script>';
        }        
    }
    
    static function css()
    {
        if (self::$css === false)
        {
            // @TODO show warning
        }
        
        foreach(array_unique(self::$css) as $path)
        {
            // Check if CSS is external (starts with //)
            if (substr($path, 0, 2) != '//')
            {
                $path = self::$frontend.$path;
            }
            echo '<link rel="stylesheet" href="'.$path.'"/>';
        }
        
        self::$css = false;
    }
    
}
?>