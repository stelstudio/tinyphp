<?php
namespace Tiny\Router;

/**
 * Interface RouteProtection
 *
 * Use interface to create classes that check if CurrentUser has access to a page
 *
 * @package Tiny
 */
interface RouteProtection
{
    public function has_access($controller, $method);
}