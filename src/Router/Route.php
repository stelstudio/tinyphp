<?php
namespace Tiny\Router;

use Tiny\Controller\Base;
use Tiny\Exception;
use Tiny\Logger;
use Tiny\Message;
use Tiny\Settings;

class Route
{
    private static $known_routes = array();

    public $controller_name;
    public $controller_path;
    public $method;
    public $base_url;
    public $regex;
    public $protection;
    public $name;

    static function startswith($url)
    {
        $route = new Route();
        $route->base_url = $url;
        return $route;
    }

    static function match($regex)
    {
        $route = new Route();
        $route->regex = $regex;
        return $route;
    }

    /**
     * Configure landing page and dispatch request
     * This must be called after all other routes
     */
    static function landing($controller, $method = false, $request_uri = false)
    {
        $route = self::startswith('/')->controller($controller);

        if ($method)
        {
            $route->method($method);
        }

//        if (!$request_uri)
//        {
//            $request_uri = $_SERVER['REQUEST_URI'];
//        }

        // Final rule should throw 404 exception
        if (!$route->dispatch($request_uri))
        {
            throw new Exception\HttpNotFoundException();
        }
    }

    static function find($name)
    {
        if (array_key_exists($name, self::$known_routes))
        {
            return self::$known_routes[$name];
        }
        else
        {
            // ...  Fail silently
            trigger_error("Route $name not found", E_USER_WARNING);
            return new Route();
        }
    }


    /**
     * Store controller name and path (optional)
     */
    function controller($name, $path = '')
    {
        $this->controller_name = strtolower($name);
        $this->controller_path = $path;
        return $this;
    }

    /**
     * Store name of a method in controller to call
     */
    function method($name)
    {
        $this->method = $name;
        return $this;
    }

    /**
     * Store function to execute to figure out if user has access
     */
    function protect($func)
    {
        $this->protection = $func;
        return $this;
    }

    /**
     * Store by name in routing table. Route can be retrieved by Route::find("name")
     */
    function name($str)
    {
        self::$known_routes[$str] = $this;
        return $this;
    }

    function url($method = '')
    {
        $r = $this->base_url;
        if ($method)
        {
            $r .= $method.'/';
        }
        return $r;
    }

    /**
     * Try to dispatch request by processing the route
     */
    function dispatch($request_uri = null)
    {
        if (!$request_uri)
        {
            list($request_uri, $qs_params) = explode('?', $_SERVER['REQUEST_URI']);
        }

        if ($this->base_url)
        {
            $base_len = strlen($this->base_url);

            if (substr($request_uri, 0, $base_len) == $this->base_url)
            {
                $method = $this->method;
                if (!$method)
                {
                    $pieces = explode('/', substr($request_uri, $base_len));
                    $method = $pieces[0];
                }

                // Execute controller's code
                if ($this->execute_controller($method))
                {
                    // Everything is ok, page rendered, terminate...
                    exit(0);
                }
            }

            return false;
        }

        if ($this->regex)
        {
            if (preg_match($this->regex, $request_uri, $matches))
            {
                if (!$this->controller_name)
                {
                    $this->controller_name = $matches['controller'];
                }
                $method = ($this->method)  ?  $this->method  :  $matches['method'];
                if ($this->execute_controller($method, $matches))
                {
                    exit(0);
                }
            }

            return false;
        }
    }

    /**
     * Load controller and execute it's method
     * @return boolean
     */
    private function execute_controller($method = '', $params = array())
    {
        $file = 'controllers/';
        if ($this->controller_path)
        {
            $file .= $this->controller_path;
        }
        $file .= strtolower($this->controller_name).'.php';

        $class_name = ucfirst($this->controller_name).'Controller';
        $method_name = ($method)  ?  $method  :  '__default';

        if (!$this->check_permissions($this->protection, $method_name))
        {
            throw new Exception\HttpForbiddenException();
        }

        try
        {
            $included = include $file;
            if (!$included)
            {
                throw new \Exception("Error including controller $file");
            }
        }
        catch (\Exception $e)
        {
            $path = (Settings::production())  ?  ''  :  "($file)";
            throw new Exception\HttpServerErrorException("Cannot load controller $class_name $path");
        }

        $controller = new $class_name();

        // Check access level of the controller
        $has_access = $this->check_permissions(function() use ($controller, $method) {
            return $controller->access($method);
        }, $method_name);

        if (!$has_access)
        {
            Message::addError('You don\'t have access to view this page');
            throw new Exception\HttpForbiddenException();
        }

        // Method does not exist
        if (!is_callable(array($controller, $method_name)))
        {
            return false;
        }

        try
        {
            $controller->$method_name($params);
        }
        catch (Exception $e)
        {
            throw new Exception\HttpServerErrorException("Cannot execute controller's method $method_name");
        }

        return true;
    }

    /**
     * Check if the route is protected and run permissions check
     * @return boolean
     */
    private function check_permissions($callback, $method)
    {
        if (!$callback)
        {
            return true;
        }

        if ($callback instanceof RouteProtection)
        {
            // Execute "has_access" of instance of RouteProtection child
            return call_user_func_array(array($callback, "has_access"), array($this->controller_name, $method));
        }

        if (is_callable($callback))
        {
            // Execute function
            return call_user_func($callback);
        }
    }

    function serialize()
    {
        $name = ($this->name)  ?  $this->name.' '  :  '';
        $url = ($this->base_url)  ?  $this->base_url  :  $this->regex;
        $method = ($this->method)  ?  $this->method  :  '__default';
        return "Route$name: $url => {$this->controller_name}->$method";
    }
}
